module "bookstack_rds_sg" {
  source = "terraform-aws-modules/security-group/aws"

  name        = "bookstackdb-FW-rules"
  description = "FW fules for BookStack RDS instance"
  vpc_id      = aws_vpc.bookstack.id

  ingress_with_cidr_blocks = [
    {
      rule        = "mysql/aurora-tcp"
      cidr_blocks = "172.16.0.0/16"
    },
  ]
}

data "aws_ssm_parameter" "bookstackpw" {
  name = "bookstackpw"
}

module "db" {
  source  = "terraform-aws-modules/rds/aws"
  version = "~> 2.0"

  identifier = "bookstackdb"

  engine            = "mariadb"
  engine_version    = "10.5.8"
  instance_class    = "db.t2.small"
  allocated_storage = 10

  name     = "bookstackdb"
  username = "bookstackuser"
  password = data.aws_ssm_parameter.bookstackpw
  port     = "3306"

  iam_database_authentication_enabled = true

  vpc_security_group_ids = [module.bookstack_rds_sg.this_security_group_id]

  maintenance_window = "Mon:00:00-Mon:03:00"
  backup_window      = "03:00-06:00"

  tags = {
    Terraform = "true"
    Name      = "bookstackdb"
  }

  # DB subnet group
  subnet_ids = [aws_subnet.privSub1.id, aws_subnet.privSub2.id]

  # DB parameter group
  family = "mariadb10.5"

  # DB option group
  major_engine_version = "10.5"

  # Snapshot name upon DB deletion
  final_snapshot_identifier = "bookstackdb"

  # Database Deletion Protection
  deletion_protection = false

  parameters = [
    {
      name  = "character_set_client"
      value = "utf8"
    },
    {
      name  = "character_set_server"
      value = "utf8"
    }
  ]

  options = [
    {
      option_name = "MARIADB_AUDIT_PLUGIN"

      option_settings = [
        {
          name  = "SERVER_AUDIT_EVENTS"
          value = "CONNECT"
        },
        {
          name  = "SERVER_AUDIT_FILE_ROTATIONS"
          value = "37"
        },
      ]
    },
  ]
}
