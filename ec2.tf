module "bookstack_ec2_sg" {
  source = "terraform-aws-modules/security-group/aws"

  name        = "bookstackec2-FW-rules"
  description = "FW fules for BookStack EC2 instance"
  vpc_id      = aws_vpc.bookstack.id

  ingress_with_cidr_blocks = [
    {
      rule        = "https-443-tcp"
      cidr_blocks = "0.0.0.0/0"
    },
    {
      rule        = "http-80-tcp"
      cidr_blocks = "0.0.0.0/0"
    },
  ]

  tags = {
    Terraform = "true"
    Name      = "bookstackec2-FW-rules"
  }
}

resource "aws_instance" "bookstack_instance" {
  ami             = "TBD"
  instance_type   = "t3.small"
  key_name        = "bookstack_key"
  security_groups = [module.bookstack_ec2_sg.this_security_group_id]

  tags = {
    Terraform = "true"
    Name      = "BookStack"
  }

  root_block_device {
    delete_on_termination = true
    encrypted             = true
    volume_size           = 20
  }
}
