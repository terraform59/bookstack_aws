resource "aws_vpc" "bookstack" {
  cidr_block       = "172.16.0.0/16"
  instance_tenancy = "default"

  tags = {
    Name = "BookStack-VPC"
  }
}

resource "aws_subnet" "pubSub" {
  vpc_id                  = aws_vpc.bookstack.id
  cidr_block              = "172.16.0.0/24"
  availability_zone       = "us-east-1a"
  map_public_ip_on_launch = true
}

resource "aws_subnet" "privSub1" {
  vpc_id                  = aws_vpc.bookstack.id
  cidr_block              = "172.16.1.0/24"
  availability_zone       = "us-east-1b"
  map_public_ip_on_launch = false
}

resource "aws_subnet" "privSub2" {
  vpc_id                  = aws_vpc.bookstack.id
  cidr_block              = "172.16.2.0/24"
  availability_zone       = "us-east-1c"
  map_public_ip_on_launch = false
}

resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.bookstack.id

  tags = {
    Name = "BookStack-VPC-IGW"
  }
}

resource "aws_eip" "nat" {
  vpc = true
}

resource "aws_nat_gateway" "ngw" {
  allocation_id = aws_eip.nat.id
  subnet_id     = aws_subnet.pubSub.id
  depends_on    = [aws_internet_gateway.igw]
}

resource "aws_route_table" "pubRT" {
  vpc_id = aws_vpc.bookstack.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }
}

resource "aws_route_table" "privRT" {
  vpc_id = aws_vpc.bookstack.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.ngw.id
  }
}

resource "aws_route_table_association" "rta-a" {
  subnet_id      = aws_subnet.pubSub.id
  route_table_id = aws_route_table.pubRT.id
}

resource "aws_route_table_association" "rta-b" {
  subnet_id      = aws_subnet.privSub1.id
  route_table_id = aws_route_table.privRT.id
}

resource "aws_route_table_association" "rta-c" {
  subnet_id      = aws_subnet.privSub2.id
  route_table_id = aws_route_table.privRT.id
}
